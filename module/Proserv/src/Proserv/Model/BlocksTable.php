<?php

namespace Proserv\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

class BlocksTable extends AbstractTableGateway {

    protected $table = 'blocks';

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new Blocks());
        $this->initialize();
    }
    
   
    public function fetchAll(Select $select = null) {
        if (null === $select)
        $select = new Select();
        $select->from($this->table);   
        $resultSet = $this->selectWith($select);
        //echo $select->getSqlString($this->adapter->getPlatform());die;
        $resultSet->buffer();
        return $resultSet;
    }
    
    public function  get_block_description($blocks_identifier){
       
    	$select = new Select();
        $select->from($this->table);
        $select->columns(array('blocks_desc' => 'blocks_desc'));
        $select->where(array('blocks_identifier' => $blocks_identifier) );
        // $select->where(array('blocks_identifier' => 'homepage_block2') );
        //echo $select->getSqlString($this->adapter->getPlatform());die;
        $resultSet = $this->selectWith($select);
        $resultSet->buffer();
        
        foreach ($resultSet  as $value){
     			
     		$block_html = $value->blocks_desc;
     		
     		}
     	//	var_dump($block_html);exit();
		
        return $block_html;
        
       //return $resultSet;

       /*$rowset = $this->select(array('blocks_identifier' => $blocks_identifier));
       $row = $rowset->current();
        if (!$row) {
			return false;
        //	throw new \Exception("Could not find row ". $id);
        }
        var_dump($row);exit();	
        return $row;
     	*/ 
   	} 
    
    
	
}
