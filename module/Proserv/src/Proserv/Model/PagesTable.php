<?php

namespace Proserv\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

class PagesTable extends AbstractTableGateway {

    protected $table = 'pages';
   
	

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype(new Pages());


        $this->initialize();
    }
    
    
   	    
    
	public function get_meta_values($identifier){
		
	 $identifier = $identifier;
        $rowset = $this->select(array('pages_identifier' => $identifier));
        $row = $rowset->current();
        if (!$row) {
          //  throw new \Exception("Could not find row $id");
        }
        return $row;	
     	
     }

    
    
    
    public function fetchAll(Select $select = null) {
        if (null === $select)
            $select = new Select();
        $select->from($this->table);   
        $resultSet = $this->selectWith($select);
        // echo $select->getSqlString($this->adapter->getPlatform());die;
        $resultSet->buffer();
        return $resultSet;
    }
    
    
    public function getPages($id) {
        $id = (int) $id;
        $rowset = $this->select(array('pages_id' => $id));
        $row = $rowset->current();
        if (!$row) {
         //   throw new \Exception("Could not find row $id");
        }
        return $row;
    }
	
    
    
	public function savePages(Pages $pages)
     {
		$added_date=date("Y-m-d H:i:s");
	
		 $data = array(
             'pages_name'  => str_replace('&nbsp;',' ',$pages->pages_name), 
             'pages_identifier'  => str_replace('&nbsp;',' ',$pages->pages_identifier), 
             'pages_desc'  => str_replace('&nbsp;',' ',$pages->pages_desc),
             'meta_title'=>str_replace('&nbsp;',' ',$pages->meta_title),
             'meta_keyword'=>str_replace('&nbsp;',' ',$pages->meta_keyword),
             'meta_description'=>str_replace('&nbsp;',' ',$pages->meta_description), 
             'language'=>$pages->language,
             'subject'=>str_replace('&nbsp;',' ',$pages->subject),
            
         );
	
         $id = (int) $pages->pages_id;
         
         if ($id == 0) {
             $this->insert($data);
             $id = $this->adapter->getDriver()->getLastGeneratedValue();
          
         } else {
             if ($this->getPages($id)) {
                 $this->update($data, array('pages_id' => $id));
             } else {
                // throw new \Exception('Pages id does not exist');
             }
         }
         return $id;
     }
     
  
    public function deletePages($id) {
        $this->delete(array('pages_id' => $id));
    }

    public function getPagesList($status) { 
        $rowset = $this->select(array('status' => $status));              
        return $this->getResultSet($rowset);
    }
    
    public function validatepages(Pages $pages)
    {
        $select = new Select();
        $select->from($this->table);
        $select->where(array('pages_identifier'=>$pages->pages_identifier,'language'=>$pages->language));
        $resultSet = $this->selectWith($select);
        $resultSet->buffer();
        return $resultSet;
	}
	
	  public function validatepagesedit(Pages $pages,$id)
    {
        $select = new Select();
        $select->from($this->table);
        $select->where(array('pages_identifier'=>$pages->pages_identifier,'language'=>$pages->language));
         $select->where->notEqualTo('pages_id',$id);
        $resultSet = $this->selectWith($select);
        $resultSet->buffer();
        return $resultSet;
	}
	
	public function validateRecord($data,$field,$table){
		$select = new \Zend\Db\Sql\Select();
		$select->from($table)
			   ->where->equalTo($field, $data->$field);
		$validator = new \Zend\Validator\Db\RecordExists($select);
		$validator->setAdapter($this->adapter);
		if ($validator->isValid($data->$field)) {
			return false;
		} 
		else {
			return true;
		}
	}	
	public function getResultSet($rowset){
		$resultSet = new ResultSet;
        $resultSet->initialize($rowset);
        $resultSet->buffer();
        return $resultSet;
	}
	
	
	
}
