<?php

namespace Proserv\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\FileInput;

class Pages implements InputFilterAwareInterface {

     public $pages_id;
     public $pages_name;
     public $pages_identifier;
     public $pages_desc;
     public $meta_title;
     public $meta_keyword;
     public $meta_description;
     public $language;
     public $subject;
    

      protected $inputFilter;
    /**
     * Used by ResultSet to pass each database row to the entity
     */
    public function exchangeArray($data) {
        $this->pages_id = (isset($data['pages_id'])) ? $data['pages_id'] : null;
        $this->pages_name = (isset($data['pages_name'])) ? $data['pages_name'] : null;
        $this->pages_identifier = (isset($data['pages_identifier'])) ? $data['pages_identifier'] : null;
        $this->pages_desc = (isset($data['pages_desc'])) ? $data['pages_desc'] : null;
        $this->meta_title = (isset($data['meta_title'])) ? $data['meta_title'] : null;
        $this->meta_keyword = (isset($data['meta_keyword'])) ? $data['meta_keyword'] : null;
        $this->meta_description = (isset($data['meta_description'])) ? $data['meta_description'] : null;
        $this->language= (isset($data['language'])) ? $data['language'] : null;
        $this->subject= (isset($data['subject'])) ? $data['subject'] : null;
        
       
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
     	//   throw new \Exception("Not used");
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            
           
            
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'pages_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
                         
            ))); 
            
              $inputFilter->add($factory->createInput(array(
                        'name' => 'pages_name',
                        'required' => false,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                      
            ))); 
             $inputFilter->add($factory->createInput(array(
                        'name' => 'pages_identifier',
                        'required' => false,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                       
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'pages_desc',
                        'required' => false,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                      
            )));
            
              $inputFilter->add($factory->createInput(array(
                        'name' => 'meta_title',
                        'required' => false,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                      
            )));
            
              $inputFilter->add($factory->createInput(array(
                        'name' => 'language',
                        'required' => false,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                      
            )));
              $inputFilter->add($factory->createInput(array(
                        'name' => 'meta_keyword',
                        'required' => false,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                      
            )));
              $inputFilter->add($factory->createInput(array(
                        'name' => 'meta_description',
                        'required' => false,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                      
            )));
			$inputFilter->add($factory->createInput(array(
                        'name' => 'subject',
                        'required' => false,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                      
            )));
            
       
            
            

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
?>
