<?php

namespace Proserv\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\FileInput;

class Blocks implements InputFilterAwareInterface {

     public $blocks_id;
     public $blocks_name;
     public $blocks_identifier;
     public $blocks_desc;
     
    protected $inputFilter;

    /**
     * Used by ResultSet to pass each database row to the entity
     */
    public function exchangeArray($data) {
        $this->blocks_id = (isset($data['blocks_id'])) ? $data['blocks_id'] : null;
        $this->blocks_name = (isset($data['blocks_name'])) ? $data['blocks_name'] : null;
        $this->blocks_identifier = (isset($data['blocks_identifier'])) ? $data['blocks_identifier'] : null;
        $this->blocks_desc = (isset($data['blocks_desc'])) ? $data['blocks_desc'] : null;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'blocks_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
                         
            ))); 
            
              $inputFilter->add($factory->createInput(array(
                        'name' => 'blocks_name',
                        'required' => true,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                      
            ))); 
             $inputFilter->add($factory->createInput(array(
                        'name' => 'blocks_identifier',
                        'required' => true,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                       
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'blocks_desc',
                        'required' => true,
                        'filters' => array(
							 array('name' => 'StringTrim'),
                        ),
                      
            )));
            
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
?>
