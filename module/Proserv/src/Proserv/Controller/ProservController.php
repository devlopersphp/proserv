<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Proserv\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Proserv\Model\Pages;



use Zend\Validator\File\Size;
use Zend\Filter\File;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;
use Zend\Http\Headers;
use Zend\EventManager\EventManagerInterface;

use Zend\Db\Sql\Sql;
use DOMDocument;
use Zend\View\Helper\HeadLink;
use Zend\View\Model\JsonModel;
 

class ProservController extends AbstractActionController
 {
	 	protected $pagesTable;
	 	protected $BlocksTable;
	 	
	 	
	  	public function checkPermission()
		{
		 $sm = $this->getServiceLocator();
		 $postservice =  $sm->get('Aclpermission\Service\PostServiceInterface');
		 $authService = $sm->get( 'zfcuser_auth_service' );
		 $postservice = $postservice->permissionRequest('pages',$authService->getIdentity( )->getId());
		 return $postservice;
		}
	
	
		
		
		
	    public function indexAction() {
			
			
		   $this->layout('layout/proserv');
		
		
	       
	       $Pages = $this->getPagesTable();$result = $Pages->fetchAll();
	       $identifier = 'home';
	      
	       if(!empty($identifier))	{
	       $metavalues = $this->getPagesTable()->get_meta_values($identifier);
	       }	else	{
	       $identifier = 'home';
	       $metavalues = $this->getPagesTable()->get_meta_values($identifier);
	       }
	       foreach ( $metavalues as $key => $value){
		   $meta[$key] = $value;
	       }
	       $pages_desc =  $this->get_pages_desc($meta['pages_desc']); 
	       $arr_blocks = $this->get_blocks_desc($pages_desc);

	        
		  $view = new ViewModel(array( 'metavalues'  => $meta , 'arr_blocks' => $arr_blocks ));
		
	   	  return $view;  
	     
	    }
		

    
	   
     public function getPagesTable()
     {
         if (!$this->pagesTable) {
             $sm = $this->getServiceLocator();
             $this->pagesTable = $sm->get('Proserv\Model\PagesTable');
         }
         return $this->pagesTable;
     }
	
  	public function getBlocksTable()
     {
         if (!$this->blocksTable) {
         	 $sm = $this->getServiceLocator();
             $this->blocksTable = $sm->get('Proserv\Model\BlocksTable');
         }
         
         return $this->blocksTable;
     }
     
     public function get_pages_desc($pages_desc){
     	
     	$dom = new DOMDocument();
		@$dom->loadHTML($pages_desc);
		$blocks = $dom->getElementsByTagName('blocks');
		$nodes = array();
		for ($i; $i < $blocks->length; $i++)	{
			
			$blocks_identifier[$i] = $blocks->item($i)->getAttribute('blocks_identifier');
			$type[$i]= $blocks->item($i)->getAttribute('type');
		    $arr_pages_desc[$i+1]= array("type"=>$type[$i] , "blocks_identifier" =>$blocks_identifier[$i]);
		    
			}
		return 	$arr_pages_desc;	
     }
     
     public function get_blocks_desc($pages_desc){
     	
     	foreach ( $pages_desc as $key => $value){
     		
     		$arr_blocks[$key] = $this->get_block_desc($value); //Array ( [type] => 1 [blocks_identifier] => homepage_block1 ) 
     	
     	} 
     	//var_dump($arr_blocks[1]);exit();
     	return $arr_blocks;

     }
     
    public function  get_block_desc($block){     
        if($block['type'] == 1 && isset($block['blocks_identifier'])){
     		$block_html['content'] =  $this->getBlocksTable()->get_block_description($block['blocks_identifier']);
     		$block_html['type'] = 1;
     		//return $block_html;
     	}else if($block['type'] == 2 && isset($block['blocks_identifier'])){
     		$block_html['content'] =  $block['blocks_identifier']; //this will contail path of template html 
     		$block_html['type'] = 2;
     		//return $block_html;
     		//echo "i will dynamic fatch from html ";exit();
     	}
     	return $block_html;
    }
    

    
     

 }
