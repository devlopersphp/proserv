<?php
namespace Showpages\Form;

 use Zend\Form\Form;

 class PagesForm extends Form
 {
     public function __construct($name = null)
     {
         // we want to ignore the name passed
         parent::__construct('pages');
         $this->setAttribute('method', 'post'); 
		 $this->setAttribute('name', 'frm');
		 $this->setAttribute('id', 'sendrequest_professional');
         
		
		 $this->add(array(
             'name' => 'pages_id',
             'type' => 'Hidden',
         ));
         $this->add(array(
             'name' => 'pages_name',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Pages Name <span style="color: red;">*</span>',
                
                 
             ),
              'attributes' => array(
                 'class' => 'form-control',
			
             ),
         ));
         $this->add(array(
             'name' => 'pages_identifier',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Pages Identifier <span style="color: red;">*</span>',
                
             ),
              'attributes' => array(
                 'class' => 'form-control',
				
             ),
         ));
         $this->add(array(
             'name' => 'meta_title',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Meta Title <span style="color: red;">*</span>',
                
             ),
              'attributes' => array(
                 'class' => 'form-control',
				
             ),
         ));
         $this->add(array(
             'name' => 'meta_keyword',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Meta Keyword <span style="color: red;">*</span>',
                
             ),
              'attributes' => array(
                 'class' => 'form-control',
				
             ),
         ));
         $this->add(array(
             'name' => 'meta_description',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Meta Description <span style="color: red;">*</span>',
                
             ),
              'attributes' => array(
                 'class' => 'form-control',
				
             ),
         ));
         
           $this->add(array(
             'name' => 'subject',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Email subject',
                
             ),
              'attributes' => array(
                 'class' => 'form-control',
				
             ),
         ));
         
          $this->add(array(
             'name' => 'language',
             'type' => 'Select',
             'options' => array(
                 'label' => 'language <span style="color: red;">*</span>',
                
             ),
              'attributes' => array(
                 'class' => 'form-control',
                 'options'=>array(
                 'en'=>'English',
                 ),
				
             ),
         ));
       
         $this->add(array(
             'name' => 'pages_desc',
             'type' => 'Textarea',
             'options' => array(
                 'label' => 'Pages Description <span style="color: red;">*</span>',
                
             ),
              'attributes' => array(
                 'class' => 'form-control',
				'id'=>'editor',
             ),
         ));
        
      
        

         $this->add(array(
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => array(
                 'value' => 'Go',
                 'id' => 'submitbutton',
                 'class' => 'btn btn-warning',
             ),
         ));
         
        
         
         // ********** Sendrequest Form ***********
         
         
         $this->add(array(
             'name' => 'req_address',
             'type' => 'Text',
	       	 	'attributes' => array( 
					'id' => 'req_address',
					'readonly'=>'readonly',	
         			 'class' => 'form-control',	
	            ), 
         ));  
        
         
       	 $this->add(array(
             'name' => 'req_latitude',
             'type' => 'Hidden',
	       	 	'attributes' => array( 
					'id' => 'req_latitude',
							
	            ), 
         ));  
         $this->add(array(
             'name' => 'req_longitude',
             'type' => 'Hidden',
         	    'attributes' => array( 
					'id' => 'req_longitude',
							
	            ), 
         ));  
        
         
        $this->add(array( 
            'name' => 'property_type_id', 
            'type' => 'Select',
            'attributes' => array( 
				'id' => 'property_type_id',
				'maxlength'	=> '2', 
                 'class' => 'form-control',
				//'onChange' => 'on_change_property_type(this.value);',		
            ), 
            'options' => array(
				'empty_option' => 'Select Property Type',
				'disable_inarray_validator' => true,
            ), 
        )); 

        $this->add(array( 
            'name' => 'clean_style_id', 
            'type' => 'Select',
            'attributes' => array( 
				'id' => 'clean_style_id',
				'maxlength'	=> '2', 
                 'class' => 'form-control',
					
            ), 
            'options' => array(
				'empty_option' => 'Select Clean Type',
				
				'value_options' => array(
                    '1' => 'Standard Clean',
                    '2' => 'Deep Clean',
                ),
                
				'disable_inarray_validator' => true,
            ), 
        )); 
        
        $this->add(array( 
            'name' => 'bedroom', 
            'type' => 'Select',
            'attributes' => array( 
				'id' => '	bedroom',
				'maxlength'	=> '2', 
                 'class' => 'form-control',
					
            ), 
            'options' => array(
				'empty_option' => 'Select Bedrooms',
				
				'value_options' => array(
                    '1' => '1',
                    '2' => '2',
            		'3' => '3',
            		'4' => '4',
            		'5' => '5',
            		'0' => 'No',
                ),
                
				'disable_inarray_validator' => true,
            ), 
        )); 
        
        
        $this->add(array( 
            'name' => 'bathroom', 
            'type' => 'Select',
            'attributes' => array( 
				'id' => 'bathroom',
				'maxlength'	=> '2', 
                 'class' => 'form-control',
					
            ), 
            'options' => array(
				'empty_option' => 'Select Bathrooms',
				
				'value_options' => array(
                    '1' => '1',
                    '2' => '2',
            		'3' => '3',
            		'4' => '4',
            		'5' => '5',
            		'0' => 'No',
                ),
                
				'disable_inarray_validator' => true,
            ), 
        )); 
        
        
         $this->add(array( 
            'name' => 'kitchen', 
            'type' => 'Select',
            'attributes' => array( 
				'id' => 'kitchen',
				'maxlength'	=> '2', 
                 'class' => 'form-control',
					
            ), 
            'options' => array(
				'empty_option' => 'Select Kitchens',
				
				'value_options' => array(
                    '1' => '1',
                    '2' => '2',
            		'3' => '3',
            		'4' => '4',
            		'5' => '5',
            		'0' => 'No',
                ),
                
				'disable_inarray_validator' => true,
            ), 
        )); 
        
        
        
         $this->add(array( 
            'name' => 'living_rooms', 
            'type' => 'Select',
            'attributes' => array( 
				'id' => 'living_rooms',
				'maxlength'	=> '2', 
                 'class' => 'form-control',
					
            ), 
            'options' => array(
				'empty_option' => 'Select Living Rooms',
				
				'value_options' => array(
                    '1' => '1',
                    '2' => '2',
            		'3' => '3',
            		'4' => '4',
            		'5' => '5',
            		'0' => 'No',
                ),
                
				'disable_inarray_validator' => true,
            ), 
        )); 
        
       
      $this->add(array( 
            'name' => 'extra_rooms', 
            'type' => 'Select',
            'attributes' => array( 
				'id' => 'extra_rooms',
				'maxlength'	=> '2', 
                 'class' => 'form-control',
					
            ), 
            'options' => array(
				'empty_option' => 'Select Extra Rooms',
				
				'value_options' => array(
                    '1' => '1',
                    '2' => '2',
            		'3' => '3',
            		'4' => '4',
            		'5' => '5',
            		'0' => 'No',
                ),
                
				'disable_inarray_validator' => true,
            ), 
        )); 
        
        
         
        $this->add(array( 
            'name' => 'work_order_details', 
            'type' => 'textarea',
            'attributes' => array( 
				'id' => 'work_order_details',
				'maxlength'	=> '100', 
                'class' => 'form-control',
				'rows'	=> '2',
				'cols' => '2',
            ), 
            
        )); 

        $this->add(array(
             'name' => 'appointment_date',
           	 'type' => 'Text',
              'attributes' => array(
                 'class' => 'form-control',
                 
         		'id'=>'appointment_date',
				 'placeholder' => 'mm/dd/yyyy', 
             ),
         ));
        
         
         $this->add(array(
             'name' => 'appointment_time',
           	 'type' => 'Text',
              'attributes' => array(
               
                'class' => 'form-control',
         		'id'=>'appointment_time',
				 
             ),
         ));
         
         $this->add(array(
             'name' => 'coupon_code',
           	 'type' => 'Text',
              'attributes' => array(
         
                 'class' => 'form-control',
         		 'id'=>'coupon_code',
	      	   ),
         ));
         
         $this->add(array(
             'name' => 'confirm_price',
           	 'type' => 'Text',
              'attributes' => array(
                 'class' => 'form-control',
         		 'id'=>'confirm_price',
         		  'placeholder' => '$', 	
	      	   ),
         ));
         
         $this->add(array( 
            'name' => 'csrf', 
            'type' => 'Csrf', 
			 'options' => array(
             'csrf_options' => array(
                     'timeout' => 3600
             	)
     		 )
        ));      
         $this->add(array(
             'name' => 'confirmrequest',
             'type' => 'Submit',
             'attributes' => array(
                 'value' => 'CONFIRM',
                 'id' => 'confirmrequest',
                 'class' => 'btn-send text-uppercase confirmrequest',
         		 //'onClick' => 'this.disabled = true',
             ),
         ));
         
         
        // ********** Sendrequest Form ***********
         
         
         
       
         
         
         

     }
 }
