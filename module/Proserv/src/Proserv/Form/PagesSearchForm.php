<?php
namespace Proserv\Form;

use Zend\Form\Form;
use \Zend\Form\Element;

class PagesSearchForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('pages');
        $this->setAttribute('class', 'navbar-form navbar-left');
        $this->setAttribute('method', 'post');


	
        $search_txt = new Element\Text('search_text');
        $search_txt->setLabel('Search')
                ->setAttribute('class', 'form-control required')
                ->setAttribute('placeholder', 'Search');
        

  


        $submit = new Element\Submit('submit');
        $submit->setValue('Search')
                ->setAttribute('class', 'btn btn-warning');


        $this->add($search_txt);
	//$this->add($title);
	
        $this->add($submit);

    }
}


    
