<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

 namespace Proserv;

 use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
 use Zend\ModuleManager\Feature\ConfigProviderInterface;
 use Proserv\Model\Pages;
 use Proserv\Model\PagesTable;
 
 use Proserv\Model\Blocks;
 use Proserv\Model\BlocksTable;
 

 
 use Zend\Db\ResultSet\ResultSet;
 use Zend\Db\Adapter\Adapter;
 class Module implements AutoloaderProviderInterface, ConfigProviderInterface
 {
     public function getAutoloaderConfig()
     {
         return array(
             'Zend\Loader\ClassMapAutoloader' => array(
                 __DIR__ . '/autoload_classmap.php',
             ),
             'Zend\Loader\StandardAutoloader' => array(
                 'namespaces' => array(
                     __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                 ),
             ),
         );
     }

     public function getConfig()
     {
         return include __DIR__ . '/config/module.config.php';
     }
      // getAutoloaderConfig() and getConfig() methods here

     // Add this method:
      public function getServiceConfig() {
        return array(
            'factories' => array(
                'Proserv\Model\PagesTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new PagesTable($dbAdapter);
                    return $table;
                },
                 'Proserv\Model\BlocksTable' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new BlocksTable($dbAdapter);
                    return $table;
                },
                
            ),
        );
    }
     

 }
