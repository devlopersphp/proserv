<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

 return array(
     'controllers' => array(
         'invokables' => array(
            'Proserv\Controller\Proserv' => 'Proserv\Controller\ProservController',
			
 			
         ),
     ),
     
      
	// The following section is new and should be added to your file
     'router' => array(
         'routes' => array(
             
         	'Proserv' => array(
                 'type'    => 'segment',
                 'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                         'controller' => 'Proserv\Controller\Proserv',
                         'action'     => 'index',
                     ),
                 ),
             ),
             
             
             
         ),
     ),

     'view_manager' => array(
		
         'template_path_stack' => array(
            'Proserv' => __DIR__ . '/../view',
			 
         ),
         
     ),
 );
